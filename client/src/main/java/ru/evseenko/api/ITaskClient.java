package ru.evseenko.api;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import ru.evseenko.entity.dto.TaskDTO;

import java.util.List;

public interface ITaskClient {

    String URI = "http://localhost:8080/task";

    @RequestLine("GET")
    @Headers({
            "Content-Type: application/json",
            "Authorization: {token}"
    })
    List<TaskDTO> findAllTasks(@Param("token") String token);

    @RequestLine("DELETE")
    @Headers("Authorization: {token}")
    void deleteAllTasks(@Param("token") String token);

    @RequestLine("GET /{id}")
    @Headers({
            "Content-Type: application/json",
            "Authorization: {token}"
    })
    TaskDTO findTaskById(
            @Param("token") String token,
            @Param("id") final String id
    );

    @RequestLine("POST")
    @Headers({
            "Content-Type: application/json",
            "Authorization: {token}"
    })
    TaskDTO addTask(@Param("token") String token, final TaskDTO task);

    @RequestLine("PUT")
    @Headers({
            "Content-Type: application/json",
            "Authorization: {token}"
    })
    TaskDTO updateTask(@Param("token") String token, final TaskDTO task);

    @RequestLine("DELETE /{id}")
    @Headers({
            "Content-Type: application/json",
            "Authorization: {token}"
    })
    void deleteTask(@Param("token") String token, final TaskDTO task);
}
