package ru.evseenko.api;

import feign.Headers;
import feign.RequestLine;
import ru.evseenko.entity.dto.ResponseDTO;
import ru.evseenko.entity.dto.UserDTO;

public interface IRegistrationClient {

    String URI = "http://localhost:8080/registration";

    @RequestLine("POST")
    @Headers("Content-Type: application/json")
    ResponseDTO registration(final UserDTO user);

    @RequestLine("DELETE")
    @Headers("Content-Type: application/json")
    ResponseDTO remove(final UserDTO user);
}
