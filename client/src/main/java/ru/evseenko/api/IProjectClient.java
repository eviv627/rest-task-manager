package ru.evseenko.api;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import ru.evseenko.entity.dto.ProjectDTO;

import java.util.List;

public interface IProjectClient {

    String URI = "http://localhost:8080/project";

    @RequestLine("GET")
    @Headers({
            "Content-Type: application/json",
            "Authorization: {token}"
    })
    List<ProjectDTO> findAllProjects(@Param("token") String token);

    @RequestLine("DELETE")
    @Headers("Authorization: {token}")
    void deleteAllProjects(@Param("token") String token);

    @RequestLine("GET /{id}")
    @Headers({
            "Content-Type: application/json",
            "Authorization: {token}"
    })
    ProjectDTO findProjectById(
            @Param("token") String token,
            @Param("id") final String id
    );

    @RequestLine("POST")
    @Headers({
            "Content-Type: application/json",
            "Authorization: {token}"
    })
    ProjectDTO addProject(
            @Param("token") String token,
            ProjectDTO project
    );

    @RequestLine("PUT")
    @Headers({
            "Content-Type: application/json",
            "Authorization: {token}"
    })
    ProjectDTO updateProject(
            @Param("token") String token,
            ProjectDTO project
    );

    @RequestLine("DELETE /{id}")
    @Headers({
            "Content-Type: application/json",
            "Authorization: {token}"
    })
    void deleteProject(
            @Param("token") String token,
            @Param("id") String id
    );
}
