package ru.evseenko.api;

import feign.Headers;
import feign.RequestLine;
import ru.evseenko.entity.dto.AuthRequest;
import ru.evseenko.entity.dto.TokenDTO;

public interface IAuthenticationClient {
    String URI = "http://localhost:8080/login";

    @RequestLine("POST")
    @Headers("Content-Type: application/json")
    TokenDTO login(final AuthRequest user);
}
