package ru.evseenko.client;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.evseenko.api.IAuthenticationClient;
import ru.evseenko.api.IRegistrationClient;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.dto.AuthRequest;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.util.FeignClientBuilder;

import java.util.UUID;

public class UserFeignClientTest {

    private static IAuthenticationClient authenticationClient;
    private static IRegistrationClient registrationClient;

    private UserDTO user;

    @BeforeClass
    public static void initClient() {
        authenticationClient = FeignClientBuilder.createClient(
                IAuthenticationClient.class,
                IAuthenticationClient.URI
        );

        registrationClient = FeignClientBuilder.createClient(
                IRegistrationClient.class,
                IRegistrationClient.URI
        );
    }

    @Before
    public void setUpTest() {
        user = new UserDTO();
        user.setId(UUID.randomUUID().toString());
        user.setLogin("LOGINNB");
        user.setPasswordHash("password hash");
        user.setRole(Role.ADMIN);
        registrationClient.registration(user);
    }

    @After
    public void tearDownTest() {
        registrationClient.remove(user);
    }

    @Test
    public void authUserTest() {
        @NotNull final AuthRequest authRequest = new AuthRequest();
        authRequest.setLogin("LOGINNB");
        authRequest.setPassword("password hash");
        Assert.assertNotNull(authenticationClient.login(authRequest).getToken());
    }
}
