package ru.evseenko.client;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.evseenko.api.IAuthenticationClient;
import ru.evseenko.api.IProjectClient;
import ru.evseenko.api.IRegistrationClient;
import ru.evseenko.api.ITaskClient;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.dto.AuthRequest;
import ru.evseenko.entity.dto.ProjectDTO;
import ru.evseenko.entity.dto.TaskDTO;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.util.DateFormatUtil;
import ru.evseenko.util.FeignClientBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class TaskFeignClientTest {

    private static IAuthenticationClient authenticationClient;
    private static IRegistrationClient registrationClient;
    private static ITaskClient taskClient;

    private UserDTO user;
    private String token;

    @BeforeClass
    public static void initClient() {
        authenticationClient = FeignClientBuilder.createClient(
                IAuthenticationClient.class,
                IAuthenticationClient.URI
        );

        registrationClient = FeignClientBuilder.createClient(
                IRegistrationClient.class,
                IRegistrationClient.URI
        );

        taskClient = FeignClientBuilder.createClient(
                ITaskClient.class,
                ITaskClient.URI
        );
    }

    @Before
    public void setUpTest() {
        user = new UserDTO();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(generateUserName());
        user.setPasswordHash("password hash");
        user.setRole(Role.ADMIN);
        registrationClient.registration(user);

        final AuthRequest authRequest = new AuthRequest();
        authRequest.setLogin(user.getLogin());
        authRequest.setPassword("password hash");
        token = authenticationClient.login(authRequest).getToken();
    }

    @After
    public void tearDownTest() {
        registrationClient.remove(user);
    }

    @Test
    public void taskCreateTest() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setId(UUID.randomUUID().toString());
        task.setName("task1");
        task.setUserId(user.getId());
        task.setDescription("desc");
        task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
        task.setStartDate(DateFormatUtil.parseIsoDate(new Date()));
        task.setEndDate(DateFormatUtil.parseIsoDate(new Date()));

        taskClient.addTask(token, task);

        @NotNull final TaskDTO actualTask =
                taskClient.findTaskById(token, task.getId());

        Assert.assertEquals(task.toString(), actualTask.toString());
    }

    @Test
    public void taskUpdateTest() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setId(UUID.randomUUID().toString());
        task.setName("Task 1");
        task.setUserId(user.getId());
        task.setDescription("desc");
        task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
        task.setStartDate(DateFormatUtil.parseIsoDate(new Date()));
        task.setEndDate(DateFormatUtil.parseIsoDate(new Date()));

        taskClient.addTask(token, task);

        task.setName("upd_Task1");
        task.setDescription("upd_desc");
        task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
        task.setStartDate(DateFormatUtil.parseIsoDate(new Date()));
        task.setEndDate(DateFormatUtil.parseIsoDate(new Date()));

        taskClient.updateTask(token, task);

        @NotNull final TaskDTO actualTask =
                taskClient.findTaskById(token, task.getId());

        Assert.assertEquals(task.toString(), actualTask.toString());
    }

    @Test
    public void taskDeleteAllTest() {
        final int tasksNum = 20;

        for (int i = 0; i < tasksNum; i++) {
            final TaskDTO task = new TaskDTO();
            task.setId(UUID.randomUUID().toString());
            task.setName("task" + i);
            task.setUserId(user.getId());
            task.setDescription("desc");
            task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
            taskClient.addTask(token, task);
        }

        taskClient.deleteAllTasks(token);

        Assert.assertEquals(new ArrayList<>(), taskClient.findAllTasks(token));
    }

    @NotNull
    private String generateUserName() {
        @NotNull final Random random = new Random();
        return "TestClientUser" + random.nextInt(999999);
    }
}
