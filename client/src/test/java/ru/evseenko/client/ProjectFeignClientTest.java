package ru.evseenko.client;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.evseenko.api.IAuthenticationClient;
import ru.evseenko.api.IProjectClient;
import ru.evseenko.api.IRegistrationClient;
import ru.evseenko.api.ITaskClient;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.dto.AuthRequest;
import ru.evseenko.entity.dto.ProjectDTO;
import ru.evseenko.entity.dto.TaskDTO;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.util.DateFormatUtil;
import ru.evseenko.util.FeignClientBuilder;
import ru.evseenko.util.Sort;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class ProjectFeignClientTest {

    private static IAuthenticationClient authenticationClient;
    private static IRegistrationClient registrationClient;
    private static IProjectClient projectClient;
    private static ITaskClient taskClient;

    private UserDTO user;
    private String token;

    @BeforeClass
    public static void initClient() {
        authenticationClient = FeignClientBuilder.createClient(
                IAuthenticationClient.class,
                IAuthenticationClient.URI
        );

        registrationClient = FeignClientBuilder.createClient(
                IRegistrationClient.class,
                IRegistrationClient.URI
        );

        projectClient = FeignClientBuilder.createClient(
                IProjectClient.class,
                IProjectClient.URI
        );

        taskClient = FeignClientBuilder.createClient(
                ITaskClient.class,
                ITaskClient.URI
        );
    }

    @Before
    public void setUpTest() {
        user = new UserDTO();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(generateUserName());
        user.setPasswordHash("password hash");
        user.setRole(Role.ADMIN);
        registrationClient.registration(user);

        final AuthRequest authRequest = new AuthRequest();
        authRequest.setLogin(user.getLogin());
        authRequest.setPassword("password hash");
        token = authenticationClient.login(authRequest).getToken();
    }

    @After
    public void tearDownTest() {
        registrationClient.remove(user);
    }

    @Test
    public void projectCreateTest() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setId(UUID.randomUUID().toString());
        project.setUserId(user.getId());
        project.setName("project1");
        project.setDescription("desc");
        project.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
        project.setStartDate(DateFormatUtil.parseIsoDate(new Date()));
        project.setEndDate(DateFormatUtil.parseIsoDate(new Date()));

        projectClient.addProject(token, project);

        @NotNull final ProjectDTO actualProject =
                projectClient.findProjectById(token, project.getId());

        Assert.assertEquals(project.toString(), actualProject.toString());
    }

    @Test
    public void projectUpdateTest() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setId(UUID.randomUUID().toString());
        project.setName("project1");
        project.setUserId(user.getId());
        project.setDescription("desc");
        project.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
        project.setStartDate(DateFormatUtil.parseIsoDate(new Date()));
        project.setEndDate(DateFormatUtil.parseIsoDate(new Date()));

        projectClient.addProject(token, project);

        project.setName("upd_project1");
        project.setDescription("upd_desc");
        project.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
        project.setStartDate(DateFormatUtil.parseIsoDate(new Date()));
        project.setEndDate(DateFormatUtil.parseIsoDate(new Date()));

        projectClient.updateProject(token, project);

        @NotNull final ProjectDTO actualProject =
                projectClient.findProjectById(token, project.getId());

        Assert.assertEquals(project.toString(), actualProject.toString());
    }

    @Test
    public void projectDeleteAllTest() {
        final int projectsNum = 20;
        final int tasksNum = 20;

        @NotNull final List<ProjectDTO> listProjectDTO = new ArrayList<>();

        for (int i = 0; i < projectsNum; i++) {
            final ProjectDTO project = new ProjectDTO();
            project.setId(UUID.randomUUID().toString());
            project.setName("project" + i);
            project.setUserId(user.getId());
            project.setDescription("desc");
            project.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
            projectClient.addProject(token, project);
            listProjectDTO.add(project);
        }

        for (int i = 0; i < tasksNum; i++) {
            final TaskDTO task = new TaskDTO();
            task.setId(UUID.randomUUID().toString());
            task.setName("project" + i);
            task.setUserId(user.getId());
            task.setDescription("desc");
            task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
            task.setProjectId(listProjectDTO.get(i).getId());
            taskClient.addTask(token, task);
        }

        projectClient.deleteAllProjects(token);

        Assert.assertEquals(new ArrayList<>(), taskClient.findAllTasks(token));
        Assert.assertEquals(new ArrayList<>(), projectClient.findAllProjects(token));
    }

    @Test
    public void projectDeleteTaskTest() {
        final int projectsNum = 20;
        final int tasksNum = 200;

        @NotNull final List<ProjectDTO> listProjectDTO = new ArrayList<>();
        @NotNull final List<TaskDTO> listTaskDTO = new ArrayList<>();

        for (int i = 0; i < projectsNum; i++) {
            final ProjectDTO project = new ProjectDTO();
            project.setId(UUID.randomUUID().toString());
            project.setName("project" + i);
            project.setUserId(user.getId());
            project.setDescription("desc");
            project.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
            projectClient.addProject(token, project);
            listProjectDTO.add(project);
        }

        for (int i = 0; i < tasksNum; i++) {
            final TaskDTO task = new TaskDTO();
            task.setId(UUID.randomUUID().toString());
            task.setName("project" + i);
            task.setUserId(user.getId());
            task.setDescription("desc");
            task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
            task.setProjectId(listProjectDTO.get(i/10).getId());
            taskClient.addTask(token, task);
            listTaskDTO.add(task);
        }

        projectClient.deleteProject(token, listProjectDTO.remove(0).getId());

        int i = 0;
        Iterator<TaskDTO> iteratorTaskDTO = listTaskDTO.iterator();
        while (i < 10) {
            if (iteratorTaskDTO.hasNext()) {
                iteratorTaskDTO.next();
                iteratorTaskDTO.remove();
            }
            i++;
        }

        @NotNull final List<ProjectDTO> actualProjects = projectClient.findAllProjects(token);
        actualProjects.sort(Sort.NAME.getComparatorDTO());
        listProjectDTO.sort(Sort.NAME.getComparatorDTO());
        @NotNull final List<TaskDTO> actualTasks = taskClient.findAllTasks(token);
        actualTasks.sort(Sort.NAME.getComparatorDTO());
        listTaskDTO.sort(Sort.NAME.getComparatorDTO());

        Assert.assertEquals(listProjectDTO.toString(), actualProjects.toString());
        Assert.assertEquals(listTaskDTO.toString(), actualTasks.toString());
    }

    @NotNull
    private String generateUserName() {
        @NotNull final Random random = new Random();
        return "TestClientUser" + random.nextInt(999999);
    }
}
