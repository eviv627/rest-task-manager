package ru.evseenko.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.evseenko.api.controller.ITaskController;
import ru.evseenko.api.service.ITaskService;
import ru.evseenko.entity.dto.TaskDTO;
import ru.evseenko.util.ControllerUtil;
import ru.evseenko.util.DateFormatUtil;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/task")
public class TaskController implements ITaskController {

    private final ITaskService taskService;

    @Autowired
    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<TaskDTO> findAllTasks() {
        return taskService.getAll(ControllerUtil.getCurrentUserId());
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteAllTasks() {
        taskService.deleteAll(ControllerUtil.getCurrentUserId());
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public TaskDTO findTaskById(@PathVariable(value = "id") final String id) {
        return taskService.get(ControllerUtil.getCurrentUserId(), id);
    }

    @PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public TaskDTO addTask(@RequestBody final TaskDTO task) {
        task.setUserId(ControllerUtil.getCurrentUserId());
        taskService.persist(ControllerUtil.getCurrentUserId(), task);
        return task;
    }

    @RequestMapping(
            method = RequestMethod.PUT,
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    public TaskDTO updateTask(@RequestBody final TaskDTO task) {
        taskService.update(ControllerUtil.getCurrentUserId(), task);
        return task;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteTask(@PathVariable(value = "id") final TaskDTO task) {
        taskService.delete(ControllerUtil.getCurrentUserId(), task);
    }
}
