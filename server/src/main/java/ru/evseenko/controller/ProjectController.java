package ru.evseenko.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.evseenko.api.controller.IProjectController;
import ru.evseenko.api.service.IProjectService;
import ru.evseenko.entity.dto.ProjectDTO;
import ru.evseenko.util.ControllerUtil;
import ru.evseenko.util.DateFormatUtil;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/project")
public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    @Autowired
    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<ProjectDTO> findAllProjects() {
        return projectService.getAll(ControllerUtil.getCurrentUserId());
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteAllProjects() {
        projectService.deleteAll(ControllerUtil.getCurrentUserId());
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ProjectDTO findProjectById(@PathVariable(value = "id") final String id) {
        return projectService.get(ControllerUtil.getCurrentUserId(), id);
    }

    @PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ProjectDTO addProject(@RequestBody final ProjectDTO project) {
        project.setUserId(ControllerUtil.getCurrentUserId());
        projectService.persist(ControllerUtil.getCurrentUserId(), project);
        return project;
    }

    @RequestMapping(
            method = RequestMethod.PUT,
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    public ProjectDTO updateProject(@RequestBody final ProjectDTO project) {
        projectService.update(ControllerUtil.getCurrentUserId(), project);
        return project;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteProject(@PathVariable(value = "id") final String id) {
        projectService.delete(
                ControllerUtil.getCurrentUserId(),
                projectService.get(ControllerUtil.getCurrentUserId(), id)
        );
    }
}
