package ru.evseenko.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.evseenko.api.controller.IRegistrationController;
import ru.evseenko.api.service.IUserService;
import ru.evseenko.controller.validate.UserValidator;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.dto.ResponseDTO;
import ru.evseenko.entity.dto.UserDTO;


@RestController
@RequestMapping(value = "/registration")
public class RegistrationController implements IRegistrationController {

    private final IUserService userService;
    private final UserValidator userValidator;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(
            IUserService userService,
            UserValidator userValidator,
            PasswordEncoder passwordEncoder
    ) {
        this.userService = userService;
        this.userValidator = userValidator;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResponseDTO registration(@RequestBody final UserDTO user) {

        userValidator.validate(user);
        user.setPasswordHash(passwordEncoder.encode(user.getPasswordHash()));
        user.setRole(Role.ADMIN);
        user.setActive(true);
        userService.persist(user);

        return ResponseDTO.ok();
    }

    @Override
    @DeleteMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResponseDTO remove(@RequestBody final UserDTO user) {
        userService.remove(user.getId());
        return ResponseDTO.ok();
    }
}
