package ru.evseenko.controller.validate;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.evseenko.api.service.IUserService;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.exception.ServiceException;

@Component
public class UserValidator{

    private final IUserService userService;

    @Autowired
    public UserValidator(IUserService userService) {
        this.userService = userService;
    }

    public boolean supports(Class<?> aClass) {
        return UserDTO.class.equals(aClass);
    }

    public void validate(Object o) {
        UserDTO user = (UserDTO) o;

        if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
            throw new ServiceException("Invalid login. Incorrect length.");
        }
        if (userService.loadUserByUsername(user.getUsername()) != null) {
            throw new ServiceException("Invalid login. Already exist.");
        }
    }
}
