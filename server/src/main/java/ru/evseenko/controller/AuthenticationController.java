package ru.evseenko.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.evseenko.api.controller.IAuthenticationController;
import ru.evseenko.api.security.IJwtTokenProvider;
import ru.evseenko.api.service.IUserService;
import ru.evseenko.entity.dto.AuthRequest;
import ru.evseenko.entity.dto.TokenDTO;
import ru.evseenko.entity.dto.UserDTO;

@RestController
@RequestMapping(value = "/login")
public class AuthenticationController implements IAuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final IJwtTokenProvider jwtTokenProvider;
    private final IUserService userService;

    @Autowired
    public AuthenticationController(
            AuthenticationManager authenticationManager,
            IJwtTokenProvider jwtTokenProvider,
            IUserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
    }

    @PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public TokenDTO login(@RequestBody final AuthRequest user) {
        try {
            final String login = user.getLogin();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, user.getPassword()));
            final UserDTO userDTO = userService.getUserByLogin(login);

            if (userDTO == null) {
                throw new UsernameNotFoundException("User with login: " + login + " not found");
            }

            final String token = "Bearer_" + jwtTokenProvider.createToken(userDTO);
            final TokenDTO tokenDTO = new TokenDTO();
            tokenDTO.setToken(token);
            return tokenDTO;
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }
    }
}
