package ru.evseenko.security;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import ru.evseenko.api.security.IJwtTokenProvider;
import ru.evseenko.api.service.IUserService;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.exception.JwtAuthenticationException;

import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Date;

@Component
public class JwtTokenProvider implements IJwtTokenProvider {

    private final String secret;
    private final long validTime;
    private final IUserService userService;

    @Autowired
    public JwtTokenProvider(
            @Value("${jwt.token.secret}") String secret,
            @Value("${jwt.token.expired}") long validTime,
            IUserService userService
    ) {
        this.secret = Base64.getEncoder().encodeToString(secret.getBytes());
        this.validTime = validTime;
        this.userService = userService;
    }

    public String createToken(final UserDTO user) {
        Claims claims = Jwts.claims().setSubject(user.getLogin());
        claims.put("id", user.getId());
        claims.put("role", user.getRole().getName());

        Date now = new Date();
        Date validity = new Date(now.getTime() + validTime);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public String resolveToken(HttpServletRequest req) {
        final String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer_")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public Authentication getAuthentication(final String token) {
        final UserDetails userDetails = this.userService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public boolean validateToken(final String token) {
        try {
            final Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);

            return !claims.getBody().getExpiration().before(new Date());

        } catch (JwtException | IllegalArgumentException e) {
            throw new JwtAuthenticationException("JWT token is expired or invalid");
        }
    }

    public String getUsername(final String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
    }

}
