package ru.evseenko.api.security;

import org.springframework.security.core.Authentication;
import ru.evseenko.entity.dto.UserDTO;

import javax.servlet.http.HttpServletRequest;

public interface IJwtTokenProvider {

    String createToken(UserDTO user);

    String resolveToken(HttpServletRequest req);

    Authentication getAuthentication(String token);

    boolean validateToken(final String token);

    String getUsername(String token);
}
