package ru.evseenko.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.cache.annotation.Cacheable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_token")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
        @NamedQuery(
                name = PersistentToken.FIND_BY_USERNAME,
                query =
                "select persistentToken from PersistentToken persistentToken " +
                "where persistentToken.username = :username",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
        )
})
public class PersistentToken {

    public static final String FIND_BY_USERNAME = "PersistentToken.findByNamePart";

    @Id
    @Column(name = "series")
    private String series;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "token", nullable = false)
    private String token;

    @Column(name = "last_used", nullable = false)
    private Date lastUsed;
}
