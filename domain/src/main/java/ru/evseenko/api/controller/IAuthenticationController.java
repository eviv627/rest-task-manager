package ru.evseenko.api.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.evseenko.entity.dto.AuthRequest;
import ru.evseenko.entity.dto.TokenDTO;


public interface IAuthenticationController {
    @PostMapping
    TokenDTO login(@RequestBody final AuthRequest user);
}
