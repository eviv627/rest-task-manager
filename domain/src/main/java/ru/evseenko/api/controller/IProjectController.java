package ru.evseenko.api.controller;


import org.springframework.web.bind.annotation.*;
import ru.evseenko.entity.dto.ProjectDTO;

import java.util.List;

public interface IProjectController {

    @GetMapping
    List<ProjectDTO> findAllProjects();

    @RequestMapping(method = RequestMethod.DELETE)
    void deleteAllProjects();

    @GetMapping(value = "/{id}")
    ProjectDTO findProjectById(@PathVariable(value = "id") final String id);

    @PostMapping
    ProjectDTO addProject(@RequestBody final ProjectDTO project);

    @RequestMapping(method = RequestMethod.PUT)
    ProjectDTO updateProject(@RequestBody final ProjectDTO project);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    void deleteProject(@PathVariable(value = "id") final String id);
}
