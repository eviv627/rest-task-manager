package ru.evseenko.api.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.evseenko.entity.dto.ResponseDTO;
import ru.evseenko.entity.dto.UserDTO;


public interface IRegistrationController {
    @PostMapping
    ResponseDTO registration(@RequestBody final UserDTO user);

    @DeleteMapping
    ResponseDTO remove(@RequestBody final UserDTO user);
}
