package ru.evseenko.api.controller;


import org.springframework.web.bind.annotation.*;
import ru.evseenko.entity.dto.TaskDTO;

import java.util.List;

public interface ITaskController {

    @GetMapping
    List<TaskDTO> findAllTasks();

    @RequestMapping(method = RequestMethod.DELETE)
    void deleteAllTasks();

    @GetMapping(value = "/{id}")
    TaskDTO findTaskById(@PathVariable(value = "id") final String id);

    @PostMapping
    TaskDTO addTask(@RequestBody final TaskDTO task);

    @RequestMapping(method = RequestMethod.PUT)
    TaskDTO updateTask(@RequestBody final TaskDTO task);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    void deleteTask(@PathVariable(value = "id") final TaskDTO task);
}
