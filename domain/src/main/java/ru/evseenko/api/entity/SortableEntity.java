package ru.evseenko.api.entity;

import org.jetbrains.annotations.Nullable;
import ru.evseenko.entity.Status;

import java.util.Date;

public interface SortableEntity {
    @Nullable
    Date getStartDate();
    @Nullable
    Date getEndDate();
    @Nullable
    Date getCreateDate();
    @Nullable
    Status getStatus();
    @Nullable
    String getName();
}
