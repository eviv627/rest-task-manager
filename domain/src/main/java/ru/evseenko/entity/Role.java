package ru.evseenko.entity;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    ADMIN("ADMIN"), USER("USER");

    @JsonCreator
    public static Role forValue(String value) {
        return Role.valueOf(value);
    }

    @JsonValue
    public static String toValue(Role role) {
        return role.getName();
    }

    @NotNull
    private String name;

    Role(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @JsonIgnore
    public String getAuthority() {
        return name;
    }
}
