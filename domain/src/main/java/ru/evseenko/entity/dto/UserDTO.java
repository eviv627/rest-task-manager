package ru.evseenko.entity.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.evseenko.entity.Role;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

@Setter
@Getter
@XmlSeeAlso({Role.class})
@XmlRootElement(name = "user")
public class UserDTO implements Serializable, UserDetails {

    public UserDTO() {
        id = "";
        login = "";
        passwordHash = "";
        password = "";
        passwordConfirm = "";
        role = Role.USER;
    }

    @NotNull
    private String id;

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @NotNull
    private String password;

    @NotNull
    private String passwordConfirm;

    @NotNull
    private Role role;

    private boolean active;

    @Override
    public String toString() {
        return "UserDTO{" +
                "id='" + id + '\'' +
                ", login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", role=" + role +
                '}';
    }

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(role);
    }

    @Override
    @JsonIgnore
    public String getUsername() {
        return login;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return active;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return active;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return active;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return active;
    }
}
