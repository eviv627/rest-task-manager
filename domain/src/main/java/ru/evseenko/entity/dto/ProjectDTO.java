package ru.evseenko.entity.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.entity.DomainDTO;
import ru.evseenko.entity.Status;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;

@Setter
@Getter
@XmlSeeAlso({Status.class})
@XmlRootElement(name = "project")
public class ProjectDTO implements Serializable, DomainDTO {

    public ProjectDTO() {
        id = "";
        name = "";
        description = "";
        startDate = "";
        endDate = "";
        createDate = "";
        status = Status.PLANNED;
        userId = "";
    }

    @NotNull
    private String id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private String startDate;

    @NotNull
    private String endDate;

    @NotNull
    private String createDate;

    @NotNull
    private Status status;

    @NotNull
    private String userId;

    @Override
    public String toString() {
        return "ProjectDTO {" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + "" +
                ", createDate=" + "" +
                ", status=" + status +
                ", userId='" + userId + '\'' +
                '}';
    }
}
