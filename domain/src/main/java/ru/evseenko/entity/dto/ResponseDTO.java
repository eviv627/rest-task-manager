package ru.evseenko.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResponseDTO {
    private Boolean success = false;

    public static ResponseDTO ok() {
        final ResponseDTO response = new ResponseDTO();
        response.setSuccess(true);
        return response;
    }
}
